import React from "react";
import BoxItem from "./components/boxItem";
import { IUseCharacterItem } from "./interface";
import { UseQueryResult } from "react-query";
import Grid from "@mui/material/Grid";
import Container from "@mui/material/Container";
import { useParams } from "react-router-dom";
import { useCharacterItem } from "@api/characterItem/characterItem.services";
import myStore from "@store/store";
import LoadingBox from "./components/loadingBox";
import { Actions, State } from "@store/interface";

const BoxCharacter: React.FC = () => {
  const { id } = useParams();

  const { setCharacterInfo, characterInfo } = myStore(
    (state: State & Actions): State & Actions => state
  );

  const { isLoading } = useCharacterItem(id, (data): void =>
    setCharacterInfo(data)
  ) as UseQueryResult<IUseCharacterItem>;

  if (isLoading) return <LoadingBox />;

  const { name, image, gender, location, created } = characterInfo?.character;

  const createdDate = created;
  const convertDate = createdDate && new Date(createdDate);

  return (
    <>
      <Container>
        <Grid
          container
          justifyContent={"center"}
          alignItems={"center"}
          height={"100vh"}
        >
          <Grid item md={5}>
            <BoxItem
              imageUrl={image}
              characterName={name}
              gender={gender}
              location={location}
              created={convertDate && convertDate.toDateString()}
            />
          </Grid>
        </Grid>
      </Container>
    </>
  );
};

export default BoxCharacter;
