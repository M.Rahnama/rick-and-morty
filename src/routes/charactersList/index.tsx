import { useCharactersList } from "@api/charactersList/charactersList.services";
import React from "react";
import BoxItem from "./components/boxItem";
import { IUseCharactersList } from "./interface";
import { UseQueryResult } from "react-query";
import Grid from "@mui/material/Grid";
import Container from "@mui/material/Container";
import Link from "@mui/material/Link";
import myStore from "@store/store";
import LoadingBox from "./components/loadingBox";
import { Box } from "@mui/material";
import styles from "./styles.module.css";
import { Actions, State, character } from "@store/interface";

const CharactersList: React.FC = () => {
  const { setList, charactersList } = myStore(
    (state): State & Actions => state
  );

  const { isLoading } = useCharactersList((data): void =>
    setList(data.characters.results)
  ) as UseQueryResult<IUseCharactersList>;

  if (isLoading) return <LoadingBox />;

  return (
    <>
      <Container maxWidth="md">
        <Grid container my={5}>
          <Box
            component="img"
            className={styles.banner}
            alt="Rick-and-morty.png"
            src="https://s30.picofile.com/file/8467333534/Rick_and_morty.png"
          />
        </Grid>
        <Grid container data-testid="characterInfo" spacing={3} my={5}>
          {charactersList.map((item: character, inx: number) => (
            <Grid
              key={inx}
              item
              md={4}
              sx={{ justifyContent: "center", width: "100%" }}
            >
              <Link href={`/${item.id}`} underline="none">
                <BoxItem
                  imageUrl={item.image}
                  characterName={item.name}
                  characterSpecies={item.species}
                />
              </Link>
            </Grid>
          ))}
        </Grid>
      </Container>
    </>
  );
};

export default CharactersList;
