export interface ICharacter {
  imageUrl?: string;
  characterName?: string;
  characterSpecies?: string;
}
export interface IResults {
  id: string;
  image: string;
  name: string;
  species: string;
}
export interface IUseCharactersList {
  characters: {
    results: {
      id: string;
      name: string;
      image: string;
      species: string;
    }[];
  };

  error: unknown;
  isLoading: boolean;
}
