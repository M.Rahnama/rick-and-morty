import { Route, Routes } from "react-router-dom";
import "./App.css";
import React from "react";
import CharactersList from "./routes/charactersList";
import CssBaseline from "@mui/material/CssBaseline";
import BoxCharacter from "@routes/character";

const App: React.FC = () => {
  return (
    <>
      <CssBaseline />
      <Routes>
        <Route path="/" element={<CharactersList />} />
        <Route path="/:id" element={<BoxCharacter />} />
      </Routes>
    </>
  );
};

export default App;
