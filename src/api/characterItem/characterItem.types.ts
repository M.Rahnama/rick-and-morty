const enum characterItemEndpointsEnum {
  getList = "https://rickandmortyapi.com/graphql",
}

export { characterItemEndpointsEnum };
