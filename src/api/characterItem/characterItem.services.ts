import { UseQueryResult, useQuery } from "react-query";
import { GraphQLClient, gql } from "graphql-request";
import { characterItemEndpointsEnum } from "./characterItem.types";
import { characterInfo } from "@store/interface";

const graphQLClient = new GraphQLClient(characterItemEndpointsEnum.getList);

export const useCharacterItem = (
  id: string | undefined,
  handlerSuccess: (data: characterInfo) => void
): UseQueryResult<unknown, unknown> => {
  return useQuery(
    "characterItem",
    async (): Promise<unknown> => {
      const characterItem = await graphQLClient.request(gql`
      query Characters {
        character(id:${id}) {
        name
        gender
        image
        created
        location {
            name
        }
    } 
 }
    `);
      return characterItem;
    },
    {
      onSuccess(data: characterInfo) {
        handlerSuccess(data);
      },
    }
  );
};
