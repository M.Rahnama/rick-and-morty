import { UseQueryResult, useQuery } from "react-query";
import { GraphQLClient, gql } from "graphql-request";
import { charactersListEndpointsEnum } from "./charactersList.types";

const graphQLClient = new GraphQLClient(charactersListEndpointsEnum.getList);

export const useCharactersList = (
  handlerSuccess: (data: any) => void
): UseQueryResult<unknown, unknown> => {
  return useQuery(
    "charactersList",
    async (): Promise<unknown> => {
      const characters = await graphQLClient.request(gql`
        query Characters {
          characters {
            results {
              id
              name
              species
              image
            }
          }
        }
      `);
      return characters;
    },
    {
      onSuccess(data) {
        handlerSuccess(data);
      },
    }
  );
};
