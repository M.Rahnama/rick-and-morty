const enum charactersListEndpointsEnum {
  getList = "https://rickandmortyapi.com/graphql",
}

export { charactersListEndpointsEnum };
